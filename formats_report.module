<?php

/**
 * Returns a list of fields which use text formats.
 *
 * @param bool $flat
 *   If true, returns a flag list of unique field names. If false, returns an
 *   array of arrays, each array with three keys: name (the field name),
 *   entity_type, and bundle.
 */
function _formats_report_get_fields($flat = FALSE) {
  $fields_list = array();
  foreach (field_info_instances() as $entity_type => $type) {
    foreach ($type as $bundle => $fields) {
      foreach ($fields as $field) {
        if (!empty($field['settings']['text_processing'])) {
          if ($flat) {
            $fields_list[] = $field['field_name'];
          }
          else {
            $fields_list[] = array(
              'name' => $field['field_name'],
              'entity_type' => $field['entity_type'],
              'bundle' => $field['bundle'],
            );
          }
        }
      }
    }
  }
  return $flat ? array_unique($fields_list) : $fields_list;
}

/**
 * Updates existing content to use preferred text format.
 *
 * Find all entities currently using text format 'magazine_wysiwyg', and update
 * them to use 'wysiwyg', instead.
 *
 * @return array
 *   Returns list of messages detailing which entities were updated.
 */
function formats_report_update_content_new_text_format() {
  // @todo allow to be set at runtime.
  $format = 'php_code';
  $return = array();

  // First, find all entities that currently use magazine_wysiwyg text format.
  // Build them into an array keyed by field.
  $found_by_field = array();
  foreach (_formats_report_get_fields(TRUE) as $field) {
    $q = new EntityFieldQuery();
    $q->fieldCondition($field, 'format', $format);
    $r = $q->execute();
    if (!empty($r)) {
      $found_by_field[$field] = $r;
    }
  }

  // Next, transform this array into keyed by entity type. This will help us
  // to more efficiently loop over the entities and update content in their
  // fields.
  $found_by_entity_type = array();
  foreach ($found_by_field as $field => $entity_types) {
    foreach ($entity_types as $type => $entities) {
      foreach ($entities as $entity_id => $entity) {
        $found_by_entity_type[$type][$entity_id][] = $field;
      }
    }
  }

  dpm(get_defined_vars());
  // Let's not actually do anything yet.
  return;

  // Finally, for each entity type, update all its entities to use new text
  // format.
  foreach ($found_by_entity_type as $type => $entities) {
    // Loop over each entity, and update "format" key for each field that needs
    // to be fixed.
    foreach ($entities as $entity_id => $fields) {
      $changed = FALSE;
      $wrapper = entity_metadata_wrapper($type, $entity_id);

      foreach ($fields as $field_name) {
        $value = $wrapper->$field_name->value();
        if (isset($value['format']) && 'magazine_wysiwyg' === $value['format']) {
          $value['format'] = 'wysiwyg';
          $wrapper->$field_name = $value;
          $changed = TRUE;
        }
      }

      // Only save the entity if we've indeed updated a field.
      if ($changed) {
        $wrapper->save();
        $return[] = t(
          'Entity ID @id of type @type has been updated.',
          array('@id' => $entity_id, '@type' => $type)
        );
      }
    }
  }

  return $return;
}
